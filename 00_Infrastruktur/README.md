# TBZ Cloud - GNS3 - Infrastruktur

## Ausgangslage
Das Informatiklernende ein eigenes leistungsfähiges Notebook mit in den Unterricht bringen, ist heutzutage eine Selbstverständlichkeit. Früher war dies nicht der Fall, weshalb in der Schule entsprechende Arbeitsstationen zur Verfügung standen. Diese Arbeitsstationen haben heute fast keine Verwendung mehr und werden Schrittweise zurückgebaut (Stand 2022). Daraus ist im 2020 die Idee entstanden diese Maschinen für eine interne eigene TBZ Cloud (LernMAAS) zu verwenden. 

Im 2021 kam die Idee, die bestehende TBZ Cloud auf derselben Technologie (LernMAAS) zu erweitern und eine Deployment für GNS3 zu entwickeln. 

## Ziele
 - Vollumfängliche Netzwerk Emulation für Lernzwecke
 - Herstellerunabhängig
 - Multi-User, Kooperatives Arbeiten
 - Emulation einem "realen" Netzwerk möglichst nahe
 - Minimaler Vorbereitungsaufwand für Lehrpersonen
 - Kein oder nur kurze Installationsaufwand für die Lernenden
 - Minimaler Wartungsaufwand
 - "Keep it simple, stupid"
 - Der bestehenden Infrastruktur (TBZ Cloud LernMAAS) möglich ähnlich

## Netzwerktopologie
![Netzwerktopologie](media/topologie.png)

Die 20 Maschinen (Cloud-HF-25 bis Cloud-HF-44) sind in einem Layer 2 Netz, mit einem IPv4 Subnetz. Eine der Maschinen (Cloud-HF-25) übernimmt die Funktion des [Regio+Rack Controllers](https://maas.io/docs/about-controllers) und stellt auf einem Webservice Dateien für das automatische Deployment bereit (Bash Skripte, GNS3 Konfiguration und Images).

![media](media/maashowitworks.png)

## Zugriff aus dem Internet
Auf dem Router (WAN Edge, siehe Topologie) sind Port Forwarding regeln angelegt für jede einzelne Maschine. Es werden jeweils zwei Ports auf die Ports 1194 und 1195 weitergeleitet. Der erste Port wird für die OpenVPN Verbindung in das GNS3 Labor verwendet, der zweite Port ist reserve. 

## Netzwerkkonfiguration pro Node
![Netzwerkkonfiguration pro Node](media/node.png)

Detailierte Netzwerkkonfiguration siehe Skirpt `networking.sh`. Das Skript für vom cloud-init Skript aufgerufen. 

Zusammenfassung:
 - OpenVPN im Tap Modus (Layer 2 VPN) erlaubt mehrere Logins mit gleichen Key (`duplicate-cn`)
 - Interne Bridge mit IP 192.168.23.1/24 (alle nodes gleiches Subnetz)
 - DHCP Server für interne Bridge (192.168.23.129 bis 192.168.23.200)
 - Diverse Zusatzeinstellungen in `sysctl.conf`, um `iptables` auf der internen Bridge zu deaktivieren.
 - Übermitttlung des Keys an den *Regio+Rack Controller*.

# GNS3 Installation pro Node
Für die Installation von GNS3 wird eine modifizierte Version des originalen Skriptes von der GNS3 Dokumentation [Install GNS3 on a remote server](https://docs.gns3.com/docs/getting-started/installation/remote-server/) verwendet. Die Netzwerkkonfiguartion wurde in die Datei `networking.sh` ausgelagert und erweitert. 

Zustäzlich lädt das Skript am Ende der Installation die GNS3 Konfiguration und die GNS3 Images vom *Ragio+Rack Controller* herunter. 

# Hardwarespezifikation Node
 - HP ProDesk 600 G1 SFF
 - Intel(R) Core(TM) i7-4790 CPU, 8 cores, 3.6 GHz
 - 16 GB RAM
 - 256 GB SSD
 - 1 GBit/s RJ45 (1000BASE-T)


# Eindrücke
![Labor Setup](media/laborsetup.jpg)
<br>*Vorbereitung der Maschinen vor dem definiten Einbau in den Serverraum*