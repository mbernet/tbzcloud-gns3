#!/bin/bash

ETH=`ip link | awk -F: '$0 !~ "lo|vir|wl|tap|docker|br|^[^0-9]"{print $2;getline}'`

ETH=`echo $ETH | sed 's/ *$//g'`

MY_IP_ADDR=`ip addr show $ETH | grep -o "inet [0-9]*\.[0-9]*\.[0-9]*\.[0-9]*" | grep -o "[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*"`

MY_IP_ADDR=(${MY_IP_ADDR[@]})

echo "My IP is $MY_IP_ADDR"

HOSTNAME=`hostname`

GNS3VERSION=`gns3server --version`

curl -F "ovpn=@/opt/cloudinitinstall/$HOSTNAME.ovpn" -F "ip=$MY_IP_ADDR" -F "name=$HOSTNAME" -F "gns3version=$GNS3VERSION" http://10.0.33.2/gns3cloudinit/up.php

echo "Notifying region+rack GNS3 addon (up.php) success"
