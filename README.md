![TBZ Cloud Logo](media/logo.png)

# TBZ Cloud - GNS3


[GNS3](https://www.gns3.com/) ist ein leistungsstarker und umfangreicher Netzwerk-Emulator, der Emulation, Konfiguration, Testing und Fehlerbehebung von virtuellen und realen Netzwerken ermöglicht. GNS3 kann verwendet werden, um kleine Topologien mit nur wenigen Geräten auf einem Laptop oder grosse Topologien mit vielen Geräten auf mehreren Servern oder sogar in der Cloud auszuführen.

GNS3 kann bei der Vorbereitung auf Zertifizierungsprüfungen wie Cisco CCNA helfen und bei der Überprüfung von Einsätzen in der realen Welt. Der ursprüngliche Entwickler hat GNS3 entwickelt, um sich auf seine CCNP-Zertifizierungen vorzubereiten. Aufgrund dieser ursprünglichen Arbeit kann GNS3 jetzt für das Lernen von einfachen und komplexen Netzwerktechnologien verwendet werden ohne die Beschaffung von teurer und platzraubender Hardware.

Für den Einsatz von GNS3 im Unterricht stellen sich mehrere Herausforderungen: 
1.	Das Einrichten der einzelnen Umgebungen mit allen notwendigen Images auf den Laptop der Lernenden ist zeitaufwändig und kürzt die effektive Lernzeit. 
2.	Die Verfügbarkeit der Images selbst: Selbst, wenn die Lehrperson alle Images hat, muss Sie eine Infrastruktur zur Verfügung stellen, um diese den Lernenden zu verteilen. 
3.	Zusammenarbeit in Gruppen: Damit mehrere Lernende an einem Projekt gemeinsam arbeiten können, wird ein zeitlich unabhängig verfügbarer Server benötigt. 
4.	Der Vorbereitungsaufwand der Lehrperson muss geringgehalten werden. 

Diese Herausforderungen hat das Projekt **TBZ Cloud - GNS3** angegangen und auf der Basis der bestehenden [TBZ Cloud (LernMAAS)](https://github.com/mc-b/lernmaas) eine Erweiterung entwickelt, die das Deployment und den effektiven Einsatz im Unterricht vereinfachen.

Auf 19 Server (Stand Januar 2023) werden per [cloud-init](https://cloudinit.readthedocs.io/en/latest/) und eines Python Scripts die GNS3 Labor Instanzen vollautomatisch deployed. Die Lehrperson kann anschliessend die VPN Keys für die Lernenden herunterladen. Pro Maschine (Node) können sich mehrere Lernende gleichzeitig einloggen und am selben oder unterschiedlichen Projekten arbeiten. Die effektive Anzahl Lernende pro Node ist abhängig vom Umfang der einzelnen Labore. Bisherige Erfahrungen haben gezeigt, dass eine Auslastung von 2 bis 4 Lernenden pro Node (vergleiche eingesetzte Hardware in der TBZ Cloud) optimal ist.

Ein Kurs der GNS3 einsetzt beginnt üblicherweise so: Die Lernenden installieren mithilfe der Bedienungsanleitung auf ihrem lokalen Rechner den [GNS3 Client](https://github.com/GNS3/gns3-gui/releases), OpenVPN und verbinden sich mit der vorbereiteten GNS3 Instanz. Da die GNS3 Instanzen in der **TBZ Cloud** bereits mit einer umfangreichen Auswahl an Softwareimages von Router und Switches diverser Hersteller ausgestattet sind, können die Lernenden gleich mit den Laborübungen beginnen. Das zeitaufwändige Zusammensuchen und installieren der Softwareimages in GNS3 entfällt. 

Lehrpersonen können Projekte in die Labore vor oder während dem Kurs laufend importieren. Weiter hat die Lehrperson die Möglichkeit sich jederzeit zur Überprüfung des Lernfortschrittes sich die die GNS3 Instanzen einzuloggen. 

Seit dem Start des Projektes im Jahr 2021 konnte GNS3 bereits durch mehrere Lehrpersonen in den Modulen [123](https://www.modulbaukasten.ch/module/123), [129](https://www.modulbaukasten.ch/module/129) und [145](https://www.modulbaukasten.ch/module/145) eingesetzt werden. Die Lernenden schätzen den einfachen Zugang, die umfangreichen Möglichkeiten und die realitätsnähe zu realen Netzwerken. 

**TBZ Cloud - GNS3** ist die perfekte Ergänzung zu den Tools wie [Cisco Packet Tracer](https://en.wikipedia.org/wiki/Packet_Tracer) und [Filius](https://www.lernsoftware-filius.de/Herunterladen). 

In diesem Repository gibt es:
 - **[Aufbau der Instruktur](00_Infrastruktur/)**
 - **Anleitungen**
   - [GNS3 Installation](02_Bedienungsanleitung/01_GNS3%20Installation/)
   - [GNS3 Einführung](02_Bedienungsanleitung/02_GNS3%20Einf%C3%BChrung/)

## Bestehende Unterlagen (laufende Weiterentwicklung)
 - [Modul 123: Serverdienste in Betrieb nehmen](https://gitlab.com/alptbz/m123)
 - [Modul 129: LAN-Komponenten in Betrieb nehmen](https://gitlab.com/ch-tbz-it/Stud/m129)
 - [VLAN Übung im Modul 145](https://gitlab.com/alptbz/m145/-/tree/master/sections/02_vlan)

## Author
Philipp Albrecht, philipp.albrecht@tbz.ch

## Links
 - [GNS3](https://www.gns3.com/)
 - [BeASwitch / BeARouter](https://github.com/muqiuq/BeASwitch)


## Lizenz
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.