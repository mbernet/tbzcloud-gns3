# TBZ Cloud - GNS3 - Bedienungsanleitungen

**Zielpublikum**: Endbenutzer (Lernende) einer **TBZ Cloud GNS3 Instanz**

---

**[01_GNS_Installation](01_GNS3%20Installation/)**

Schritt für Schritt Anleitungen für:
 - Installation von OpenVPN
 - Installation von GNS3

---

**[02_GNS3 Einführung](02_GNS3%20Einf%C3%BChrung/)**

 - **Zwei VPCs pingen sich gegenseitig**: <br>Diese Anleitung ist gut geeignet um die ersten Schritte mit GNS3 zu lernen. 
 - **Mein PC pingt ins Labor**:<br> Durch die Verwendung eines Layer2 VPNs in das Labor, kann der persönliche Laptop genauso ins GNS3 Labor eingebunden werden, wie wenn bei einem physischen Labor der eigene Laptop in einen Switch eingesteckt wird. Diese Übung zeigt in einem einfachen Setup wie das eingesetzt werden kann. 

---

# Nützliche Tools

## Terminal Fenster für Windows: Solar Putty
Vorteil dieser Applikation: Alle Terminal fenster werden mit Tabs in einem Fenster geöffnet. Das umschalten zwischen Terminal und GNS3 ist damit viel effizienter. 
 - https://www.solarwinds.com/free-tools/solar-putty

# Hinweise MacOS
Alle Unterlagen sind für die Verwendung mit Windows vorbereitet. 

Für macOS gibt es keine Anleitungen oder Support. Für häufig aufgetretene Probleme wurden nachfolgen aufgeführt.

## OpenVPN auf macOs - Tunnelblick

OpenVPN unter macOS: [Tunnelblick](https://tunnelblick.net/)

Beachte, dass Apple in den neueren Systemen den Zugriff auf das System immer weiter einschränkt. Tunnelblick hat entsprechende Informationen hier zusammengestellt:
 - https://tunnelblick.net/cTunTapConnections.html
 - https://tunnelblick.net/cCatalina.html
 - https://tunnelblick.net/cMonterey.html



