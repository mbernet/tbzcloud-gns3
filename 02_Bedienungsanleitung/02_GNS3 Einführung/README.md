# GNS3 Einführung <!-- omit in toc -->

# Inhaltsverzeichnis  <!-- omit in toc -->
- [1. Zwei VPCs pingen sich gegenseitig](#1-zwei-vpcs-pingen-sich-gegenseitig)
- [2. Mein PC pingt ins Labor](#2-mein-pc-pingt-ins-labor)
  - [2.1. Beispieldokumentation](#21-beispieldokumentation)

# 1. Zwei VPCs pingen sich gegenseitig
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  1/2 Lektion |
| Ziele | Sie sind in der Lage ein GNS3 Projekt mit zwei VPCS zu erstellen, die sich gegenseitig pingen können. |

![GNS3 Screenshot zweigt zwei VPCS](images/2VPCS.png)

## 1.1. Vorgehen  <!-- omit in toc -->
Schauen Sie dieses Video an und bauen Sie das Labor in GNS3 nach. 

![GNS3 Video Zwei VPCS pingen](videos/GNS3_Intro_zwei_vpcs_ping.webm)

## 1.2. Hinweise  <!-- omit in toc -->
 - Das Subnetz 192.168.23.0/24 ist gegeben. 
 - Die IP Adresse 192.168.23.1/24 ist für den GNS3 drei Server reserviert. Diese Adresse dürfen Sie **NIE** verwenden, sonst verliert die GNS3 GUI die Verbindung zum GNS3-Server.
 - Im Subnetz 192.168.23.0/24 ist ein DHCP Server aktiv, der IP-Adressen ab 192.168.23.128 bis 192.168.23.200 vergibt. Diesen IP-Range sollten sie nicht statisch vergeben. 


# 2. Mein PC pingt ins Labor
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  1/2 Lektion |
| Ziele | Sie sind in der Lage mit ihren PC ein Gerät in einem anderen Subnetz im GNS3 Labor anzupingen. |

Die GNS3 Labore der TBZ sind so aufgebaut, dass sich die Lernenden mit einem Layer2-VPN Tunnel zum GNS3 Labor verbinden. Layer2-VPN werden im produktiven Umfeld nur noch selten verwendet, da alle Broadcasts über alle VPN Verbindungen geschickt werden und die Performance unter anderem deshalb schlechter als ein Layer3-VPN ist. 

Der Einsatz des Layer2 VPNs hat jedoch den grossen Vorteil, dass sie ihren Computer virtuell in einem Gerät innerhalb des Labors verbinden können. Es wäre, als ob sie ihren PC mit einem Netzwerkkabel mit einem Router oder Switch verbinden. 

In dieser Übung wird gezeigt, wie direkt vom eigenen PC in das Labor "gepingt" werden kann. 

![GNS3 Screenshot Mein PC pingt ins Labor](images/mypcinthelab.PNG)

## 2.1. Vorgehen <!-- omit in toc -->
Schauen Sie dieses Video an und bauen Sie das Labor in GNS3 nach. 

![GNS3 Video Ins Labor pingen](videos/GNS3_Mit_meinem_PC_ins_Labor_pingen.webm)

## 2.2. Hinweise <!-- omit in toc -->
 - Das Subnetz 192.168.24.0/24 ist zufällig gewählt. Sie können auch ein anderes [Privates Subnetz](https://de.wikipedia.org/wiki/Private_IP-Adresse) auswählen. 

## 2.1. Beispieldokumentation
Für die Leistungsbeurteilung sind alle durchgeführten Schritte zu dokumentieren. 

Es wird empfohlen dies im Markdown-Format zu machen.

Der erwartete Umfang können Sie an der Beispieldokumentation ableiten: [Beispiel Dokumentation.md](Beispiel%20Dokumentation.md)